@extends('Layouts.app')

@section('title', 'User on-boarding process chart')

@section('css')

    <style type="text/css">
        #container {
            min-width: 310px;
            max-width: 800px;
            height: 400px;
            margin: 0 auto
        }
    </style>

@endsection

@section('content')

    <div id="container"></div>

@endsection

@section('scripts')

    <script type="text/javascript">

        Highcharts.chart('container', {

            title: {
                text: 'User on-boarding flow'
            },

            subtitle: {
                text: 'Temper Data Source'
            },

            yAxis: {
                title: {
                    text: 'Number of user percentage %'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 1
                }
            },

            series: [
                    @foreach( $chartData as $week )
                {
                    name: "Week {{ $week->week }}",
                    data: [{{ $week->levelOne }}, {{ $week->levelTwo }}, {{ $week->levelThree }}, {{ $week->levelFour }}, {{ $week->levelFive }}, {{ $week->levelSix }}, {{ $week->levelSeven }}, {{ $week->levelEight }}]
                },
                @endforeach
            ],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    </script>

@endsection