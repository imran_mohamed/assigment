<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->readCSV();
    }

    public function readCSV()
    {
        $file = fopen(public_path().'/export.csv', 'r');
        while (($line = fgetcsv($file)) !== FALSE) {
            $user_id = $line[0];
            $date = $line[1];
            $onboarding_perentage = $line[2];
            $count_applications = $line[3];
            $count_accepted_applications = $line[4];

            $user = new \App\User();
            $user->id = $user_id;
            $user->date = $date;
            $user->onboarding_perentage = $onboarding_perentage;
            $user->count_applications = $count_applications;
            $user->count_accepted_applications = $count_accepted_applications;
            $user->save();

        }
        fclose($file);
    }
}
