<?php

namespace App\Http\Controllers;

use App\Business\Classes\Users\Onboarding;
use App\Business\Classes\Util;
use App\User;


class DashBoardController extends Controller
{
    public function index()
    {
        $dateStart = User::min('date');
        $dateEnd = User::max('date');

        $util = new Util();
        $weeks = $util->chuckToWeeks( $dateStart, $dateEnd );

        $onboarding = new Onboarding();
        $chartData = $onboarding->weeklyOnboardingPercentages($weeks);

        return view('welcome', compact('chartData'));



    }
}
