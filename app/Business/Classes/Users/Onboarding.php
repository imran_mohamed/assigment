<?php
/**
 * Created by PhpStorm.
 * User: imran
 * Date: 3/21/18
 * Time: 12:47 PM
 */

namespace App\Business\Classes\Users;


use App\User;

class Onboarding
{

    public function weeklyOnboardingPercentages( $weeks )
    {
        $chartData = array();

        foreach ( $weeks as $key => $week )
        {
            $data = (object)NULL;
            $users = User::whereIn('date', $week)->get();
            $totalUsers = $users->count('id');
            $data->week = $key;
            $data->weekDays = json_encode($week);
            $data->levelOne = 100;
            $data->levelTwo = ($users->where('onboarding_perentage', '>=', 20)->count('id') / $totalUsers) * 100;
            $data->levelThree = ($users->where('onboarding_perentage', '>=',40)->count('id') / $totalUsers) * 100;
            $data->levelFour = ($users->where('onboarding_perentage', '>=',50)->count('id') / $totalUsers) * 100;
            $data->levelFive = ($users->where('onboarding_perentage', '>=',70)->count('id') / $totalUsers) * 100;
            $data->levelSix = ($users->where('onboarding_perentage', '>=',90)->count('id') / $totalUsers) * 100;
            $data->levelSeven = ($users->where('onboarding_perentage', '>=',99)->count('id') / $totalUsers) * 100;
            $data->levelEight = ($users->where('onboarding_perentage',100)->count('id') / $totalUsers) * 100;

            array_push($chartData, $data);
        }

        return $chartData;
    }
}