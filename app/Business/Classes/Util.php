<?php
/**
 * Created by PhpStorm.
 * User: imran
 * Date: 3/20/18
 * Time: 11:19 PM
 */

namespace App\Business\Classes;

class Util
{

    public function chuckToWeeks( $dateStart, $dateEnd )
    {
        $start = new \DateTime($dateStart);
        $end = new \DateTime($dateEnd." 23:59:00");
        $interval = new \DateInterval('P1D');
        $dateRange = new \DatePeriod($start, $interval, $end);

        $weekNumber = 1;
        $weeks = array();
        foreach ($dateRange as $date) {
            $weeks[$weekNumber][] = $date->format('Y-m-d');
            if ($date->format('w') == 6) {
                $weekNumber++;
            }
        }

        return $weeks;
    }


}